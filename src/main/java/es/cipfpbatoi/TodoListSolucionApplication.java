package es.cipfpbatoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoListSolucionApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodoListSolucionApplication.class, args);
    }

}
