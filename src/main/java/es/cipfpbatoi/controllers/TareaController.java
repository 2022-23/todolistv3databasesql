package es.cipfpbatoi.controllers;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Categoria;
import es.cipfpbatoi.modelo.entidades.Tarea;
import es.cipfpbatoi.modelo.repositorios.TareaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TareaController {

    @Autowired
    private TareaRepository tareaRepository;

    @GetMapping(value = "/tarea-del")
    @ResponseBody
    public String deleteAction(@RequestParam int codTarea) throws NotFoundException {
        Tarea tarea = tareaRepository.get(codTarea);
        tareaRepository.remove(tarea);
        return "<html><p>Tarea Borrada " + codTarea + " con éxito<p>" +
                "<p><a href=\"/\">Volver al menu principal</a></p><html>";
    }

    @GetMapping("/tarea")
    public String getTareaAction(@RequestParam int codTarea, Model modelAndView) throws NotFoundException {
        Tarea tarea = tareaRepository.getWithCategories(codTarea);
        modelAndView.addAttribute("tarea", tarea);
        modelAndView.addAttribute("usuarioLogueado", "Alex");
        return "tarea_details_view";
    }

    @GetMapping("/tarea/add")
    public String tareaFormAction(Model model){
        ArrayList<Categoria> categorias = tareaRepository.findAllCategories();
        model.addAttribute("categorias", categorias);
        return "tarea_form_view";
    }

    @PostMapping(value = "/tarea/add")
    public String postAddAction(@RequestParam Map<String, String> params) {
        String user = params.get("user");
        String descripcion = params.get("description");
        String prioridad = params.get("priority");
        String realizadaString = params.get("realizada");
        boolean realizada = realizadaString != null && realizadaString.equalsIgnoreCase("true");
        Tarea.Priority priority = Tarea.Priority.fromText(prioridad);
        String horaExpiracionString = params.get("expiration_time");
        String fechaExpiracionString = params.get("expiration_date");
        int categoriaId = Integer.parseInt(params.get("categoria_id"));
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime fechaHora = LocalDateTime.parse(fechaExpiracionString + " " + horaExpiracionString, timeFormatter);
        Categoria categoria = new Categoria(categoriaId);
        Tarea tarea = new Tarea(user, descripcion, priority, fechaHora, realizada, categoria);
        tareaRepository.save(tarea);
        return "redirect:/tareas";
    }

    @GetMapping(value = "/tareas")
    public String tareaListAction(Model model, @RequestParam HashMap<String, String> params) {
        String usuario = (params.get("usuario") != null && !params.get("usuario").isBlank()) ? params.get("usuario"): null;
        Boolean realizado = (params.get("realizada") != null && !params.get("realizada").isBlank()) ?
                Boolean.parseBoolean(params.get("realizada")): null;
        LocalDate vencimiento = (params.get("vencimiento") != null && !params.get("vencimiento").isBlank()) ?
                LocalDate.parse(params.get("vencimiento"), DateTimeFormatter.ofPattern("yyyy-MM-dd")) : null;
        ArrayList<Tarea> tareas;
        if (usuario != null || realizado != null || vencimiento != null) {
            tareas = tareaRepository.findAll(realizado, vencimiento, usuario);
        } else {
            tareas = tareaRepository.findAll();
        }
        model.addAttribute("tareas", tareas);
        return "list_tarea_view";
    }

    @GetMapping(value = "/tareas/categorias")
    public String tareaListWithCategoryAction(Model model) {
        ArrayList<Tarea> tareas = tareaRepository.findAllWithCategories();
        model.addAttribute("tareas", tareas);
        return "list_tarea_with_categories_view";
    }

}
