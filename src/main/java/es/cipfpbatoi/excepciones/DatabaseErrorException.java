package es.cipfpbatoi.excepciones;

public class DatabaseErrorException extends RuntimeException {

    public DatabaseErrorException(String message) {
        super(message);
    }

}
