package es.cipfpbatoi.modelo.entidades;

import java.time.LocalDateTime;
import java.util.InputMismatchException;
import java.util.Objects;

public class Tarea {

    public enum Priority {

        ALTA, BAJA, MEDIA;

        public static Priority fromText(String value){
            if (value.equals("alta")) {
                return Priority.ALTA;
            } else  if (value.equals("baja")) {
                return Priority.BAJA;
            } else  if (value.equals("media")) {
                return Priority.MEDIA;
            }
            throw new InputMismatchException("El valor " + value + " es inválido");
        }
    }

    private int codigo;

    private String usuario;

    private String descripcion;

    private LocalDateTime creadoEn;

    private Priority prioridad;

    private LocalDateTime vencimiento;

    private boolean realizada;

    private Categoria categoria;

    public Tarea(int codigo) {
       this.codigo = codigo;
    }
    public Tarea(String usuario, String descripcion, Priority priority, LocalDateTime vencimiento, boolean realizada, Categoria categoria) {
        this(-1, usuario, descripcion, LocalDateTime.now(), priority, vencimiento, realizada, categoria);
    }
    public Tarea(int codigo, String usuario, String descripcion, Priority priority, LocalDateTime vencimiento, boolean realizada, Categoria categoria) {
        this(codigo, usuario, descripcion, LocalDateTime.now(), priority, vencimiento, realizada, categoria);
    }

    public Tarea(int codigo, String usuario, String descripcion, LocalDateTime creadoEn, Priority priority, LocalDateTime vencimiento, boolean realizada, Categoria categoria) {
        this.codigo = codigo;
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.prioridad = priority;
        this.creadoEn = creadoEn;
        this.vencimiento = vencimiento;
        this.realizada = realizada;
        this.categoria = categoria;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalDateTime getCreadoEn() {
        return creadoEn;
    }

    public Priority getPrioridad() {
        return prioridad;
    }

    public LocalDateTime getVencimiento() {
        return vencimiento;
    }

    public int getCodigo() {
        return codigo;
    }

    public boolean isRealizada() {
        return realizada;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tarea)) {
            return false;
        }
        Tarea tarea = (Tarea) o;
        return getCodigo() == tarea.getCodigo();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCodigo());
    }
}
