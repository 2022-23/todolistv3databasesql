package es.cipfpbatoi.modelo.repositorios;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.dao.SQLCategoriaDAO;
import es.cipfpbatoi.modelo.dao.SQLTareaDAO;
import es.cipfpbatoi.modelo.dao.interfaces.CategoriaDao;
import es.cipfpbatoi.modelo.dao.interfaces.TareaDao;
import es.cipfpbatoi.modelo.entidades.Categoria;
import es.cipfpbatoi.modelo.entidades.Tarea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
public class TareaRepository {

    private TareaDao tareaDao;

    private CategoriaDao categoriaDao;

    public TareaRepository(@Autowired SQLTareaDAO tareaDao,
                           @Autowired SQLCategoriaDAO categoriaDao) {
        this.tareaDao = tareaDao;
        this.categoriaDao = categoriaDao;
    }

    /**
     * Añade la Tarea recibida como argumento a la base de datos en memoria
     * @param tarea
     */
    public void save(Tarea tarea) {
        if (tarea.getCodigo() < 0) {
            this.tareaDao.add(tarea);
        } else {
            this.tareaDao.update(tarea);
        }
    }

    /**
     * Obtiene la Tarea con codigo @codTarea. En caso de que no la encuentre devolverá una excepción
     * @NotFoundException
     *
     * @param codigo
     */
    public Tarea get(int codigo) throws NotFoundException {
        return tareaDao.getById(codigo);
    }

    /**
     * Obtiene la Tarea y su agregado Categoria. En caso de que no la encuentre devolverá una excepción
     * @NotFoundException
     *
     * @param codigo
     */
    public Tarea getWithCategories(int codigo) throws NotFoundException {
        Tarea tarea = get(codigo);
        Categoria categoria = categoriaDao.getById(tarea.getCategoria().getId());
        tarea.setCategoria(categoria);
        return tarea;
    }

    public Tarea find(int codTarea) {
        return tareaDao.findById(codTarea);
    }

    /**
     *  Devuelve el listado de todas las tareas.
     */
    public ArrayList<Tarea> findAll() {
        return tareaDao.findAll();
    }

    /**
     *  Devuelve el listado de todas las tareas.
     */
    public ArrayList<Tarea> findAll(Boolean isRealizado, LocalDate vencimiento, String usuario) {
        return tareaDao.findAllWithParams(isRealizado, vencimiento, usuario);
    }

    /**
     *  Devuelve el listado de todas las tareas cuyo atributo nombre coincide con @user
     */
    public ArrayList<Tarea> findAll(String user) {
        return tareaDao.findAllWithParams(user);
    }

    public ArrayList<Tarea> findAllWithCategories() {
        ArrayList<Tarea> tareas =  findAll();
        for (Tarea item: tareas) {
            int categoriaId = item.getCategoria().getId();
            Categoria categoriaPoblada = categoriaDao.findById(categoriaId);
            item.setCategoria(categoriaPoblada);
        }
        return tareas;
    }

    public ArrayList<Categoria> findAllCategories() {
        return categoriaDao.findAll();
    }

    /**
     * Borra una tarea a partir de su código
     * @param tarea
     */
    public void remove(Tarea tarea) {
        this.tareaDao.delete(tarea.getCodigo());
    }

    public int nextCode() {
       ArrayList<Tarea> tareas = tareaDao.findAll();
        int max = 0;
        for (Tarea item: tareas) {
            max = Math.max(max, item.getCodigo());
        }
        return max + 1;
    }
}
