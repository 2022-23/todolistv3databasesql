package es.cipfpbatoi.modelo.dao.interfaces;

import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.entidades.Categoria;
import es.cipfpbatoi.modelo.entidades.Tarea;

import java.time.LocalDate;
import java.util.ArrayList;

public interface CategoriaDao {

    ArrayList<Categoria> findAll();

    Categoria findById(int id);

    Categoria getById(int codigo) throws NotFoundException;

}




