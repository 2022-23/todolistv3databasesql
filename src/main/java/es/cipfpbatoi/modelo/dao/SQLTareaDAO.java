package es.cipfpbatoi.modelo.dao;

import es.cipfpbatoi.excepciones.DatabaseErrorException;
import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.dao.interfaces.TareaDao;
import es.cipfpbatoi.modelo.entidades.Categoria;
import es.cipfpbatoi.modelo.entidades.Tarea;
import es.cipfpbatoi.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class SQLTareaDAO implements TareaDao {

    private final MySQLConnection mySQLConnection;

    public SQLTareaDAO(@Autowired MySQLConnection mySQLConnection) {
        this.mySQLConnection = mySQLConnection;
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id o lanza una excepción si no encuentra
     * una tarea con @id
     *
     * @param id
     * @return
     */
    @Override
    public Tarea getById(int id) throws NotFoundException {
        Tarea tarea = findById(id);
        if (tarea == null) {
            throw new NotFoundException("La tarea con código " + id + " no existe");
        }
        return tarea;
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id
     *
     * @param id
     * @return
     */
    @Override
    public Tarea findById(int id) {
        String sql = "SELECT * FROM tareas WHERE codigo = ? ";
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
        ){
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return mapToTarea(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve todas las tareas
     * @return
     */
    @Override
    public ArrayList<Tarea> findAll() {
        try (
                Connection connection = mySQLConnection.getConnection();
                Statement ps = connection.createStatement();
        ){
            String sql = "SELECT * FROM tareas";
            ArrayList<Tarea> tareas = new ArrayList<>();
            ResultSet rs = ps.executeQuery(sql);
            while (rs.next()) {
                tareas.add(mapToTarea(rs));
            }
            return tareas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve las tareas cuyo atributo realizado coincide con @isRealizada y la fecha de vencimiento coincide @fecha
     * Si alguno de los atributos es null no deberá tenerse en cuenta
     *
     * @return Listado de tareas
     */
    @Override
    public ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate vencimiento, String usuario) {

        try (
                Connection connection = mySQLConnection.getConnection();
                Statement ps = connection.createStatement();
        ){
            ArrayList<Tarea> tareas = new ArrayList<>();
            String sql = "SELECT * FROM tareas WHERE ";
            StringBuilder where = new StringBuilder();
            if (isRealizada != null) {
                where.append(String.format("realizada = %d ", (isRealizada) ? 1 : 0));
            }
            if (vencimiento != null) {
                where.append(String.format("%s date(vencimiento) LIKE '%s%%' ", (where.length() > 0) ? "AND" : "", vencimiento));
            }
            if (usuario != null) {
                where.append(String.format("%s usuario LIKE '%%%s%%' ", (where.length() > 0) ? "AND" : "", usuario));
            }
            ResultSet rs = ps.executeQuery(sql + where);
            while (rs.next()) {
                tareas.add(mapToTarea(rs));
            }
            return tareas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve todas las tareas cuyo usuario coincide con @usuario
     *
     * @param usuario
     * @return Listado de tareas
     */
    @Override
    public ArrayList<Tarea> findAllWithParams(String usuario) {
        String sql = "SELECT * FROM tareas WHERE usuario = ? ";
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
        ){
            ArrayList<Tarea> tareas = new ArrayList<>();
            ps.setString(1, usuario);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tareas.add(mapToTarea(rs));
            }
            return tareas;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Añade una tarea al final del archivo siguiendo el formato
     *
     *  5;Batoi;Jugar al badminton;2022-01-06 15:00:00;BAJA;2023-04-06 15:00:00:true
     *
     * @param tarea
     * @return boolean
     */
    @Override
    public void add(Tarea tarea) {
        String sql = "INSERT INTO tareas (codigo, usuario, descripcion, fechaCreacion, vencimiento, prioridad, realizada, categoria_id) " +
                "VALUES (?,?,?,?,?,?,?,?) ";
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
        ){
            ps.setNull(1, Types.INTEGER);
            ps.setString(2, tarea.getUsuario());
            ps.setString(3, tarea.getDescripcion());
            ps.setTimestamp(4, Timestamp.valueOf(tarea.getCreadoEn()));
            ps.setTimestamp(5, Timestamp.valueOf(tarea.getVencimiento()));
            ps.setString(6, tarea.getPrioridad().toString().toUpperCase());
            ps.setBoolean(7, tarea.isRealizada());
            ps.setInt(8, tarea.getCategoria().getId());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                int autoIncremental = rs.getInt(1);
                tarea.setCodigo(autoIncremental);
            }

        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    @Override
    public void update(Tarea tarea) {
        String sql = "UPDATE tareas SET usuario = ?, " +
                "descripcion = ?, fechaCreacion = ?, vencimiento = ?, prioridad = ?," +
                " realizada = ?, categoria_id = ? WHERE codigo = ?";
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
        ) {
           ps.setString(1, tarea.getUsuario());
           ps.setString(2, tarea.getDescripcion());
           ps.setTimestamp(3, Timestamp.valueOf(tarea.getCreadoEn()));
           ps.setTimestamp(4, Timestamp.valueOf(tarea.getVencimiento()));
           ps.setString(5, tarea.getPrioridad().toString().toUpperCase());
           ps.setBoolean(6, tarea.isRealizada());
           ps.setInt(7, tarea.getCategoria().getId());
           ps.setInt(8, tarea.getCodigo());
           ps.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     *  Borra la tarea cuyo atributo codigo coincide con @codigo. El algoritmo más sencillo es:
     *
     *  1. Leer todo el fichero y almacenar el contenido en un listado de tipo ArrayList<Tarea>
     *  2. Eliminar La tarea cuyo código coincide con @codigo del listado
     *  3. Abrir el archivo para reescribirlo
     *  4. Guardar el listado de tareas completo en el fichero
     *
     * @param codigo
     */
    @Override
    public void delete(int codigo) {
        String sql = "DELETE FROM tareas WHERE codigo = ? ";
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
        ){
            ps.setInt(1, codigo);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    private Tarea mapToTarea(ResultSet resultSet) throws SQLException {
        int cod = resultSet.getInt("codigo");
        String usuario = resultSet.getString("usuario");
        String descripcion = resultSet.getString("descripcion");
        LocalDateTime creadoEn = resultSet.getTimestamp("fechaCreacion").toLocalDateTime();
        Tarea.Priority priority = Tarea.Priority.fromText(resultSet.getString("prioridad").toLowerCase());
        LocalDateTime vencimiento = resultSet.getTimestamp("vencimiento").toLocalDateTime();
        boolean realizado = resultSet.getBoolean("realizada");
        int categoriaId = resultSet.getInt("categoria_id");
        Categoria categoria = new Categoria(categoriaId);
        return new Tarea(cod, usuario, descripcion, creadoEn, priority, vencimiento, realizado, categoria);
    }

}
