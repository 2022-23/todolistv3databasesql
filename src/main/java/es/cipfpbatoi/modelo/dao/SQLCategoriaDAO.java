package es.cipfpbatoi.modelo.dao;

import es.cipfpbatoi.excepciones.DatabaseErrorException;
import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.dao.interfaces.CategoriaDao;
import es.cipfpbatoi.modelo.entidades.Categoria;
import es.cipfpbatoi.modelo.entidades.Tarea;
import es.cipfpbatoi.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;

@Service
public class SQLCategoriaDAO implements CategoriaDao {

    private final MySQLConnection mySQLConnection;

    public SQLCategoriaDAO(@Autowired MySQLConnection mySQLConnection) {
        this.mySQLConnection = mySQLConnection;
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id o lanza una excepción si no encuentra
     * una tarea con @id
     *
     * @param id
     * @return
     */
    @Override
    public Categoria getById(int id) throws NotFoundException {
        Categoria categoria = findById(id);
        if (categoria == null) {
            throw new NotFoundException("La categoria con código " + id + " no existe");
        }
        return categoria;
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id
     *
     * @param id
     * @return
     */
    @Override
    public Categoria findById(int id) {
        String sql = "SELECT * FROM categorias WHERE id = ? ";
        try (
                Connection connection = mySQLConnection.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
        ){
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return mapToEntity(rs);
            }
            return null;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve todas las tareas
     * @return
     */
    @Override
    public ArrayList<Categoria> findAll() {
        try (
                Connection connection = mySQLConnection.getConnection();
                Statement ps = connection.createStatement();
        ){
            String sql = "SELECT * FROM categorias";
            ArrayList<Categoria> categorias = new ArrayList<>();
            ResultSet rs = ps.executeQuery(sql);
            while (rs.next()) {
                categorias.add(mapToEntity(rs));
            }
            return categorias;
        } catch (SQLException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    private Categoria mapToEntity(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String nombre = resultSet.getString("nombre");
        String icono = resultSet.getString("icono");
        String descripcion = resultSet.getString("descripcion");
        return new Categoria(id, nombre, icono, descripcion);
    }

}
