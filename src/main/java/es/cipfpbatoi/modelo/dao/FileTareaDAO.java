package es.cipfpbatoi.modelo.dao;

import es.cipfpbatoi.excepciones.DatabaseErrorException;
import es.cipfpbatoi.excepciones.NotFoundException;
import es.cipfpbatoi.modelo.dao.interfaces.TareaDao;
import es.cipfpbatoi.modelo.entidades.Categoria;
import es.cipfpbatoi.modelo.entidades.Tarea;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@Service
public class FileTareaDAO implements TareaDao {
    private static final String DATABASE_FILE = "/databases/tareas.txt";
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final int FIELD_CODE = 0;
    public static final int FIELD_USER = 1;
    public static final int FIELD_DESCRIPTION = 2;
    public static final int FIELD_CREADO_EN = 3;
    public static final int FIELD_PRIORITY = 4;
    public static final int FIELD_VENCIMIENTO = 5;
    public static final int FIELD_IS_REALIZADA = 6;
    public static final int FIELD_CATEGORIA_ID = 7;

    private final File file;

    public FileTareaDAO() {
        this.file = new File(getClass().getResource(DATABASE_FILE).getFile());
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id o lanza una excepción si no encuentra
     * una tarea con @id
     *
     * @param id
     * @return
     */
    @Override
    public Tarea getById(int id) throws NotFoundException {
        Tarea tarea = findById(id);
        if (tarea == null) {
            throw new NotFoundException("La tarea con código " + id + " no existe");
        }
        return tarea;
    }

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id
     *
     * @param id
     * @return
     */
    @Override
    public Tarea findById(int id) {
        try (BufferedReader bufferedReader = getReader()){
            do {
                String register = bufferedReader.readLine();
                if (register == null) {
                    return null;
                } else if (!register.isBlank()) {
                    Tarea tarea = mapToTarea(register);
                    if (tarea.getCodigo() == id) {
                        return tarea;
                    }
                }
            } while (true);
        } catch (IOException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve todas las tareas
     * @return
     */
    @Override
    public ArrayList<Tarea> findAll() {
        ArrayList<Tarea> tareas = new ArrayList<>();
        try (BufferedReader bufferedReader = getReader()){
            do {
                String register = bufferedReader.readLine();
                if (register == null) {
                    return tareas;
                } else if (!register.isBlank()) {
                    Tarea tarea = mapToTarea(register);
                    tareas.add(tarea);
                }
            } while (true);
        } catch (IOException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve las tareas cuyo atributo realizado coincide con @isRealizada y la fecha de vencimiento coincide @fecha
     * Si alguno de los atributos es null no deberá tenerse en cuenta
     *
     * @param isRealizada
     * @param fecha
     * @return
     */
    @Override
    public ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate fecha, String usuario) {
        ArrayList<Tarea> tareas = new ArrayList<>();
        try (BufferedReader bufferedReader = getReader()){
            do {
                String register = bufferedReader.readLine();
                if (register == null) {
                    return tareas;
                } else if (!register.isBlank()) {
                    Tarea tarea = mapToTarea(register);
                    if (isRealizada != null && tarea.isRealizada() != isRealizada) {
                        continue;
                    }
                    if (fecha != null && !tarea.getVencimiento().toLocalDate().equals(fecha)) {
                        continue;
                    }
                    if (usuario != null && !tarea.getUsuario().startsWith(usuario)) {
                        continue;
                    }
                    tareas.add(tarea);
                }
            } while (true);
        } catch (IOException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Devuelve todas las tareas cuyo usuario coincide con @usuario
     *
     * @param usuario
     * @return
     */
    @Override
    public ArrayList<Tarea> findAllWithParams(String usuario) {
        ArrayList<Tarea> tareas = new ArrayList<>();
        try (BufferedReader bufferedReader = getReader()){
            do {
                String register = bufferedReader.readLine();
                if (register == null) {
                    return tareas;
                } else if (!register.isBlank()) {
                    Tarea tarea = mapToTarea(register);
                    if (tarea.getUsuario().toLowerCase().contains(usuario.toLowerCase())) {
                        tareas.add(tarea);
                    }
                }
            } while (true);
        } catch (IOException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    /**
     * Añade una tarea al final del archivo siguiendo el formato
     *
     *  5;Batoi;Jugar al badminton;2022-01-06 15:00:00;BAJA;2023-04-06 15:00:00:true
     *
     * @param tarea
     * @return boolean
     */
    @Override
    public void add(Tarea tarea) {
        try (BufferedWriter bufferedWriter = getWriter(true)){
            bufferedWriter.newLine();
            String register = mapToRegister(tarea);
            bufferedWriter.write(register);
        } catch (IOException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    @Override
    public void update(Tarea tarea) {
        throw new RuntimeException("Not implemente yet");
    }

    /**
     *  Borra la tarea cuyo atributo codigo coincide con @codigo. El algoritmo más sencillo es:
     *
     *  1. Leer todo el fichero y almacenar el contenido en un listado de tipo ArrayList<Tarea>
     *  2. Eliminar La tarea cuyo código coincide con @codigo del listado
     *  3. Abrir el archivo para reescribirlo
     *  4. Guardar el listado de tareas completo en el fichero
     *
     * @param codigo
     */
    @Override
    public void delete(int codigo) {
        ArrayList<Tarea> tareas = findAll();
        Tarea tarea = new Tarea(codigo);
        tareas.remove(tarea);
        try (BufferedWriter bufferedWriter = getWriter(false)){
            for (int i = 0; i < tareas.size(); i++) {
                bufferedWriter.write(mapToRegister(tareas.get(i)));
                if (i != (tareas.size() - 1)) {
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            throw new DatabaseErrorException(e.getMessage());
        }
    }

    private BufferedReader getReader() throws IOException {
        FileReader fileReader = new FileReader(this.file);
        return new BufferedReader(fileReader);
    }

    private BufferedWriter getWriter(boolean append) throws IOException {
        FileWriter fileWriter = new FileWriter(this.file, append);
        return new BufferedWriter(fileWriter);
    }

    private Tarea mapToTarea(String register) {
        String[] fields = register.split(";");
        int cod = Integer.parseInt(fields[FIELD_CODE]);
        String usuario = fields[FIELD_USER];
        String descripcion = fields[FIELD_DESCRIPTION];
        LocalDateTime creadoEn = LocalDateTime.parse(fields[FIELD_CREADO_EN],
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Tarea.Priority priority = Tarea.Priority.fromText(fields[FIELD_PRIORITY].toLowerCase());
        LocalDateTime vencimiento = LocalDateTime.parse(fields[FIELD_VENCIMIENTO],
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        boolean realizado = Boolean.parseBoolean(fields[FIELD_IS_REALIZADA]);
        Integer categoriaId = Integer.parseInt(fields[FIELD_CATEGORIA_ID]);
        Categoria categoria = new Categoria(categoriaId);
        return new Tarea(cod, usuario, descripcion, creadoEn, priority, vencimiento, realizado, categoria);
    }

    private String mapToRegister(Tarea tarea) {
        return String.format("%d;%s;%s;%s;%s;%s;%b;%d",
                tarea.getCodigo(), tarea.getUsuario(), tarea.getDescripcion(),
                tarea.getCreadoEn().format(dateTimeFormatter), tarea.getPrioridad().toString().toUpperCase(),
                tarea.getVencimiento().format(dateTimeFormatter), tarea.isRealizada(), tarea.getCategoria().getId());
    }

}
